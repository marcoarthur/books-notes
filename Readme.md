# PostgreSQL First Experience

This is the collection of my solutions to some queries in this book. We try to
make all development plan and implementation of the queries to be scrutinized
in order to say if this query runs in optimal or suboptimal, is readable, is sensible,
can be better documented and how to implement API based implementation.
