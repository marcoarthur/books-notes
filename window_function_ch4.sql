/*
For each ticket, display all the included flight segments together with
connection time. Limit the results to the tickets booked a week ago.
*/

SELECT      tf.ticket_no
            , f.departure_airport
            , f.arrival_airport
            , f.scheduled_arrival
            , lead(f.scheduled_departure) OVER w AS next_departure -- time to next
            , lead(f.scheduled_departure) OVER w - f.scheduled_arrival AS gap -- connection time
FROM        bookings b
            JOIN tickets t ON (t.book_ref = b.book_ref)
            JOIN ticket_flights tf ON (tf.ticket_no = t.ticket_no)
            JOIN flights f ON (tf.flight_id = f.flight_id)
WHERE       b.book_date = bookings.now()::date - INTERVAL '7 day'
WINDOW W    AS (PARTITION BY tf.ticket_no ORDER BY f.scheduled_departure)
ORDER BY    gap
;

/*
Which are the most frequent combinations of first and last names ? What is the
ratio of the passengers with such names to the total number of passengers ?
*/

SELECT      passenger_name
            , round( 100.0 * cnt / sum(cnt) OVER(), 2 ) as percent
FROM        (
            SELECT      passenger_name, count(*) cnt
            FROM        tickets
            GROUP BY    passenger_name
            ) t
ORDER BY percent DESC
;













-- this creates table(i,r,t) where i:index and r: random value (0,1), t: a type
-- a char value to define 'categorization'.
select      i
            , r 
            , case      when r between 0 and 0.3 then 'a' 
                        when r between 0.3 and 0.6 then 'b'
                        when r between 0.6 and 0.9 then 'c' 
                        else 'd'
            end t
from        generate_series(1,10) f(i)
            , lateral( select  random() + 0*i as r) s
;

select s.i, s.r from generate_series(1,10) ii, lateral (values (ii, random())) as s(i,r);

-- example of window function in practice, using partition by clause
with random_t AS  --random_t(x,y) x: 1..10, y: 'a'..'d'
(
            select x, chr(ascii('a') + (random()*3)::int) y
            from generate_series(1,10) f(x)
) select    x
            , y
            , sum(x) over w
            , count(x) over w
            , avg(x) over w
from random_t
window w AS ( partition by y )
;
