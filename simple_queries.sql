/*
 * simple queries from PostGresql first experience
 * Chapter "Demo Database"
 */

SELECT  flight_id, model
        , aircraft_code
FROM    flights NATURAL INNER JOIN aircrafts
WHERE   departure_airport = 'SVO'
AND     arrival_airport = 'OVB'
;

/*
Who traveled from Moscow (SVO) to Novosibirsk (OVB) on seat 1A the day before
yesterday, and when was the ticket booked ?
*/

-- book solution
SELECT  t.passenger_name
        , b.book_date
        , f.flight_no flight_code
        , f.scheduled_departure
        , bp.seat_no
FROM    tickets t
JOIN    bookings b
ON      t.book_ref = b.book_ref
JOIN    boarding_passes bp
ON      bp.ticket_no = t.ticket_no
JOIN    flights f
ON      f.flight_id = bp.flight_id
WHERE   f.departure_airport = 'SVO'
AND     f.arrival_airport = 'OVB'
AND     f.scheduled_departure::date = bookings.now()::date - INTERVAL '2 day'
AND     bp.seat_no = '1A'
;

-- mine solution
SELECT t.passenger_name, b.book_date
FROM bookings b 
NATURAL JOIN tickets t 
NATURAL JOIN boarding_passes bp 
NATURAL JOIN flights f
WHERE f.departure_airport = 'SVO'
AND f.arrival_airport = 'OVB'
AND f.scheduled_departure::date = bookings.now()::date - INTERVAL '2 day'
AND bp.seat_no = '1A'
;

-- How many *seats* remained free on flight PG0404 yesterday ?
-- query 1:

-- All seats in flight specified flight
SELECT count(*) total_free_seats
FROM  flights f
NATURAL JOIN aircrafts a
NATURAL JOIN seats s
WHERE flight_no = 'PG0404'
AND f.scheduled_departure::date = bookings.now()::date - INTERVAL '1 day'
AND s.seat_no NOT IN(
    -- All booked seats
    SELECT bp.seat_no
    FROM ticket_flights tf
    NATURAL JOIN flights f
    NATURAL JOIN boarding_passes bp
    WHERE flight_no = 'PG0404'
    AND f.scheduled_departure::date = bookings.now()::date - INTERVAL '1 day'
)
;

-- More readable version of query 1,

-- the book solution is trick
-- flights and seats table contains the aircraft code,
-- so it join all seats from the flight joining seats and flights from
-- the aircraft_code they share.
-- and returning null for the occupied seats on boarding_passes of the
-- fligtht.
SELECT count(*) total_free_seats
FROM    flights f -- flights
JOIN    seats s -- seats
ON      s.aircraft_code = f.aircraft_code -- relation by aircraft_code
WHERE   f.flight_no = 'PG0404'
AND     f.scheduled_departure::date = bookings.now()::date - INTERVAL '1 day'
AND NOT EXISTS (
    SELECT NULL
    FROM boarding_passes bp
    WHERE bp.flight_id = f.flight_id
    AND bp.seat_no = s.seat_no
)
;

/*
Which flights had the longest delays ? Print the list of ten "leaders"
*/

-- mine solution, good observation is to confirm that actual departure is not
-- null (flights booked and yet to be realized).
SELECT      flight_no
            , actual_departure - scheduled_departure as delay
FROM        flights f
WHERE       actual_departure IS NOT NULL
ORDER by    delay DESC
LIMIT       10
;

/*
What is the shortest flight duration for each possible flight from Moscow to
St. Petersburg, and how many times was the flight delayed for more than an
hour?
*/

-- mine (wrong solution)
SELECT  f.flight_no,
        f.actual_arrival - f.actual_departure  as flight_duration
FROM    flights f JOIN airports a 
        ON ( f.departure_airport = a.airport_code )
WHERE   a.city = 'Moscow' 
        AND f.arrival_airport IN (
            SELECT  airport_code
            FROM    airports a
            WHERE   a.city = 'St. Petersburg'
        ) 
        AND f.status = 'Arrived';
;

-- the book solution
SELECT      f.flight_no                     -- the group id for flight
            , f.scheduled_duration          -- the scheduled duration
            , min(f.actual_duration)        -- the min travel time
            , max(f.actual_duration)        -- the max travel time
            , sum(
                CASE WHEN f.actual_departure > f.scheduled_departure + INTERVAL '1 hour'
                    THEN 1 ELSE 0 END
            ) delays -- how many delays greater than an hour
FROM        flights_v f
WHERE       f.departure_city = 'Moscow'
            AND f.arrival_city = 'St. Petersburg'
            AND f.status = 'Arrived'
GROUP BY f.flight_no, f.scheduled_duration
;

/*
The lesson learned:
Aggregate functions can receive an expression as argument.

The sum() aggregate is filled with a CASE expression that gives a handy way to
treat more complicated business logic. In this case, to answer how many flights
was delayed for more than an hour.

*/

/*

Find the most disciplined passengers who checked in first for all their
flights. Take into account only those passengers who took at least two flights.

*/

--  Mine

SELECT      t.passenger_name || ' ' || t.passenger_id name_id
            , count(*) total_flights

FROM        tickets t
GROUP BY    name_id
--HAVING      total_flights >= 2
ORDER BY    total_flights DESC
LIMIT       10
;

-- Book
SELECT      t.passenger_name
            , t.ticket_no
FROM        tickets t
            JOIN boarding_passes bp ON bp.ticket_no = t.ticket_no
GROUP BY    t.passenger_name,
            t.ticket_no
HAVING      max(bp.boarding_no) = 1
            and count(*) > 1
;

-- explore how to collect the critical data on the previous select, I mean,
-- the grouping and the having parts into the select list.
WITH passenger_tickets AS (
    SELECT      t.passenger_name as name
                , t.ticket_no
                , count(*) as total_tickets
                , array_agg(t.ticket_no) as ticket_numbers
    FROM        tickets t
                JOIN boarding_passes bp ON bp.ticket_no = t.ticket_no
    GROUP BY    t.passenger_name, t.ticket_no
    HAVING      max(bp.boarding_no) = 1
)
SELECT  row_to_json(pt)
FROM    passenger_tickets pt
WHERE   pt.total_tickets > 1
;


SELECT      t.passenger_name
            , t.ticket_no
            , count(*) as total_tickets
FROM        tickets t
            JOIN boarding_passes bp ON bp.ticket_no = t.ticket_no
GROUP BY    t.passenger_name,
            t.ticket_no
HAVING      max(bp.boarding_no) = 1
            and count(*) > 1
ORDER BY total_tickets
;
