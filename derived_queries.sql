/*

By flight number, compute the avg free seats.

*/

WITH flight_vacant_seat AS (
            SELECT  f.flight_no
                    , count(*) free_seats
                    , f.actual_departure::date as flight_date
            FROM    flights f -- flights
            JOIN    seats s -- seats
            ON      s.aircraft_code = f.aircraft_code
            WHERE   NOT EXISTS (
                SELECT NULL
                FROM boarding_passes bp
                WHERE bp.flight_id = f.flight_id
                AND bp.seat_no = s.seat_no
            )
                    AND f.actual_departure IS NOT NULL
            GROUP BY f.flight_no, f.actual_departure::date
), avg_fseats AS (
            SELECT      flight_no
                        , avg(free_seats)
            FROM        flight_vacant_seat fvs
            GROUP BY    flight_no
)
SELECT      DISTINCT f.flight_no
            , af.avg as avg_freeseats
            , f.departure_city
            , f.arrival_city
FROM        flights_v f
            JOIN avg_fseats af
            ON( af.flight_no = f.flight_no )
ORDER BY avg_freeseats ASC
;


/*
Compute the number of flight for each passenger
*/

WITH passengers_ticket AS (
            SELECT      passenger_id as id
                        , passenger_name as name
                        , count(*) as total_tickets
                        , array_agg(ticket_no) as tickets_number
            FROM        tickets
            GROUP BY    passenger_id, passenger_name
) 
SELECT      name
            , id
            , total_tickets
FROM        passengers_ticket
WHERE       total_tickets > 1
ORDER BY    total_tickets DESC
;

-- SELECT      f.flight_no
--             , f.departure_city
--             , f.arrival_city
-- FROM        flights_v f
-- WHERE       flight_no IN (
--             SELECT      flight_no
--             FROM        flight_vacant_seat fvs
--             GROUP BY    flight_no
--             HAVING      avg(free_seats) <= 4
--             )
-- ;
--avg(free_seats) over (partition by flight_no, EXTRACT(MONTH FROM flight_date))
